import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ready-scan',
  templateUrl: './ready-scan.component.html',
  styleUrls: ['./ready-scan.component.scss']
})
export class ReadyScanComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  scanData() {
    this.router.navigate(['/main/home'])
  }
}
