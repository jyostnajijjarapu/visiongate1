import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadyScanComponent } from './ready-scan.component';

describe('ReadyScanComponent', () => {
  let component: ReadyScanComponent;
  let fixture: ComponentFixture<ReadyScanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReadyScanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadyScanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
