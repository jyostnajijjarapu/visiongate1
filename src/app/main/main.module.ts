import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MaterialModule } from 'src/assets/materialmodule';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MainRoutingModule, MaterialModule
  ]
})
export class MainModule { }
