import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { timer } from 'rxjs';
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { environment } from 'src/environments/environment';
import { WebsocketService } from 'src/app/websocket.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  // details = [
  //   { 'box id': '6789H', 'Part number': '983938A', 'boxQty': 7, 'partQty': 20 },
  //   { 'box id': '6779K', 'Part number': '983938A', 'boxQty': 7, 'partQty': 20 },
  //   { 'box id': '6799I', 'Part number': '983938A', 'boxQty': 7, 'partQty': 20 },
  //   { 'box id': '6785M', 'Part number': '983938A', 'boxQty': 7, 'partQty': 20 },

  // ];
  jobId = 1234;
  timestamp: any;
  date: any;
  socket: any;
  imgsrc: any = ''
  detailsData: any = [];
  boxCount: any;
  shift: any;
  saveDBorNot = '0';
  ws: WebSocket;

  constructor(private httpClient: HttpClient, private sanitizer: DomSanitizer, private socketService: WebsocketService) {

    var timestamp = new Date().getTime();

    this.ws = new WebSocket('ws://127.0.0.1:8000/test');

    // this.ws.onopen = () => this.ws.send("Connected from client");
    this.ws.onmessage = (event) => {

      console.log(event.data, "event")
      this.detailsData = JSON.parse(event.data);
      this.boxCount = this.detailsData.length;
      this.imgsrc = this.detailsData[0]['image_url']
      // this.imgsrc = this.imgsrc.substring(62)
      this.imgsrc = `assets/Images/${this.imgsrc}`

      // var el: any = document.getElementById("testimg");
      // el.src = "assets/Images/box.png?t=" + timestamp;

      console.log(this.imgsrc)
      this.jobId = this.detailsData[0]['job_id']
      this.ws.send(this.saveDBorNot)
    }


  }

  ngOnInit(): void {
    timer(0, 1000).subscribe(() => {
      this.date = new Date();
      // this.timestamp = Math.floor(Date.now() / 1000)

      const hours = this.date.getHours();
      if (hours >= 6 && hours < 15) {
        this.shift = 1
      }
      else if (hours >= 15 && hours < 24) {
        this.shift = 2;
      }
      else {
        this.shift = 3
      }
    })
  }
  sendTOdb() {
    this.saveDBorNot = '1'
    // var body = [];
    // for (let i = 0; i < this.detailsData.length; i++) {
    //   let obj = { Box_Id: this.detailsData[i]['box_id'], Part_Number: this.detailsData[i]['part_nu'], Box_Qty: this.detailsData[i]['quantity'], Shift: this.shift }
    //   body.push(obj);
    // }
    // this.socketService.sendMessage(body)
    // this.ws.send(JSON.stringify(body));

    // this.httpClient.post('create_dolley', body).subscribe((res) => {
    //   console.log(res)
    // })
  }
  sanitize(url: string): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

}
