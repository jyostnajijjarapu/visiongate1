import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { menuItems } from '../sideMenu';
import { timer } from 'rxjs';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  menuOptions: any = JSON.parse(JSON.stringify(menuItems));
  isMenuOpen = false;
  date: any;
  shift: any;
  constructor(private router: Router) {

  }

  ngOnInit(): void {
    timer(0, 1000).subscribe(() => {
      this.date = new Date();
      // this.timestamp = Math.floor(Date.now() / 1000)
      // this.timestamp = new Date().setSeconds(0, 0);
      const hours = this.date.getHours();
      if (hours >= 6 && hours < 15) {
        this.shift = 1
      }
      else if (hours >= 15 && hours < 24) {
        this.shift = 2;
      }
      else {
        this.shift = 3
      }
    })


  }
  toggleMenu(isOpened?: boolean) {
    if (isOpened) {
      this.isMenuOpen = true;
    } else {
      this.isMenuOpen = !this.isMenuOpen;
      // this.accordion.closeAll();
    }
  }
  applyActiveLinkBasedOnRoute(menuItem: any) {
    return this.router.url.includes(menuItem.routerLink);
  }
}
