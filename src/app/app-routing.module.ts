import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './main/home/home.component';
import { ReadyScanComponent } from './ready-scan/ready-scan.component';

const routes: Routes = [
  // { path: 'home', component: HomeComponent },
  { path: '', component: ReadyScanComponent },
  { path: 'palletScan', component: ReadyScanComponent },
  {
    path: 'main', loadChildren: () => import('./main/main.module').then(m => m.MainModule),
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
