import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { webSocket } from 'rxjs/webSocket';
import { io } from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  ws!: WebSocket;
  socketIsOpen = 1;

  createObservableSocket(url: string): Observable<any> {
    this.ws = new WebSocket(url);

    return new Observable(
      observer => {

        this.ws.onmessage = (event) =>
          observer.next(event.data);

        this.ws.onerror = (event) => observer.error(event);

        this.ws.onclose = (event) => observer.complete();

        return () =>
          this.ws.close(1000, "The user disconnected");
      }
    );
  }

  sendMessage(message: any) {
    if (this.ws.readyState === this.socketIsOpen) {
      this.ws.send(JSON.stringify(message));
      return `Sent to server ${message}`;
    } else {
      return 'Message was not sent - the socket is closed';
    }
  }

  // socket: any;
  // readonly uri: string = 'ws://127.0.0.1:8000/test'
  // constructor() {
  //   this.socket = io(this.uri)
  // }

  // listen() {
  //   return new Observable((subscriber: any) => {
  //     this.socket.on('/test', (data: any) => {
  //       subscriber.next(data)
  //     })
  //   })
  // }
}
